package refactorizacionPassword;

import java.util.Scanner;

public class RefactorPassword {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

Scanner input = new Scanner(System.in);
		
		mostrarMenu();

		System.out.println("Introduce la longitud de la cadena: ");
		int longitud = input.nextInt();
		
		System.out.println("Elige tipo de password: ");
		int opcion = input.nextInt();
		
		String password = generarPassword(longitud, opcion);
		
		System.out.println(password);
		input.close();
	}
	
	/**
	 * 
	 * @return
	 */
	
	public static char generarLetraNumeroOCaracterEspecial() {
		
		
		int n;
		
		n = (int) (Math.random() * 3);
		
		if (n == 1) {
			
			return letraAleatoria();
			
		} else if (n == 2) {
			
			char caracter4;
			
			caracter4 = generarCaracterEspecialAleatorio();
			
			return caracter4;
			
		} else {
			
			int numero4;
			
			numero4 = generarCifraAleatoria();
			
			password += numero4;
		}
		
	}
	
	/**
	 * 
	 * @param longitud
	 * @param opcion
	 * @return
	 */
	
	public static String generarPassword(int longitud, int opcion) {
		String password = "";
		
		for (int i = 0; i < longitud; i++) {
			
		switch (opcion) {
		case 1:
				password += letraAleatoria();
		
			break;
			
		case 2:
				password += generarCifraAleatoria();
				
			break;
			
		case 3:
			password += generarLetraOCarecterEspecial();
			
			break;
		case 4:
			password += generarLetraNumeroOCaracterEspecial();
			break;
			
			}
		}
		return password;
		
	}
	
	/**
	 * 
	 * @return
	 */
	
	public static char generarLetraOCarecterEspecial() {
		
		int n;
		
		n = (int) (Math.random() * 2);
		
		if (n == 1) {
			
			return letraAleatoria();
			
		} else {
			
			return generarCaracterEspecialAleatorio();
		}
	}

	/**
	 * 
	 * @return
	 */
	
	public static char generarCifraAleatoria() {
		
		return (char) (Math.random() * 10);
	}

	/**
	 * 
	 * @return
	 */
	
	public static char generarCaracterEspecialAleatorio() {
		
		return (char) ((Math.random() * 15) + 33);
	}

	/**
	 * 
	 * @return nos retorna una letra
	 */
	
	public static char letraAleatoria() {
		
		return (char) ((Math.random() * 26) + 65);
	}

	/**
	 * Con este metodo unicamente mostramos un menu
	 * 
	 */
	
	public static void mostrarMenu() {
		
		System.out.println("Programa que genera passwords de la longitud indicada, y del rango de caracteres");
		System.out.println("1 - Caracteres desde A - Z");
		System.out.println("2 - Numeros del 0 al 9");
		System.out.println("3 - Letras y caracteres especiales");
		System.out.println("4 - Letras, numeros y caracteres especiales");
	}

}


