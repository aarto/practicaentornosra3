package refactorizacion;

//Se debe llamar a la clase entera

import java.util.Scanner;
/**
 * 
 * @author fvaldeon
 * @since 31-01-2018
 */
 
public class Refact2 {

	// Un escaner nunca sera una final static
	
	//final static Scanner input;
	
	public static void main(String[] args) 
	{
	
		// Esta mal declarado porque debe ponerse un Scanner antes de la a y no puede llamarse a
		Scanner input = new Scanner(System.in);
		
		
		//Las variables deberian estar cada una en una linea y la variable "n" no nos dice nada 
		//Ni tampoco se usa en el programa por lo tanto no nos haria falta
		
		int n;
		int cantidad_maxima_alumnos;
		
		cantidad_maxima_alumnos = 10;
		
		//El array esta mal declarado, los corchetes deben ir detras del int, ademas este aray no nos dice que hace
		//Mas adelante nos dice que es un array de notas asi que le cambiamos el nombre
		
		int[] arraysNotas = new int[10];
		
		//Dentro del bucle for debe escribirse primero int y luego el resto del contenido ademas deberiamos llamar i
		// a la variable del int "i"
		
		for(int i = 0; i < 10; i++)
		{
			//Debemos escribir correctamente el nombre del scanner, ademas que lee numeros, no caracteres ni cadenas
			//Por lo tanto hay que poner nextInt
			System.out.println("Introduce nota media de alumno");
			arraysNotas[i] = input.nextInt();
		}	
		
		System.out.println("El resultado es: " + recorrer_array(arraysNotas));
		
		input.close();
	}
	//El corchete tiene que estar justo detras del parentesis
	static double recorrer_array(int vector[]){
		//No debemos llamar a las variables con letras
		double numero = 0;
		
		//La variable "int" del for, debemos ponerla como una i
		for(int i = 0; i < 10; i++) 
		{
			numero = numero + vector[i];
		}
		return numero/10;
	}
	
}